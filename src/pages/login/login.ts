import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ViewController,AlertController, Platform  } from 'ionic-angular';
import { UsuarioProvider } from "../../providers/usuario/usuario";
import { HomePage } from '../../pages/home/home';
import { Storage } from '@ionic/storage';
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

    constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams    ,
  	public loadingCtrl: LoadingController,
    public viewCtrl: ViewController,
    private _us: UsuarioProvider,
    private alertCtrl: AlertController,
    public platform: Platform,
    private storage: Storage) {
  }
 
  email:string;
  password:string; 
  public token: any;
  iniciar_sesion(){  
    
    this._us.loginUser(this.email, this.password)
        .subscribe( 
            res => {

          
              if(this.platform.is("cordova")){ 
                console.log(res);
                const array = res;
                  this.storage.set('token', (array['sessionTokenBck']+''));  
                  this.storage.set(`email`, this.email);
                  this.token = this.storage.get("token")
                  console.log(this.token);
                  this.navCtrl.setRoot(HomePage);
              }else {
                  console.log(res);
                  const array = res;
                  localStorage.setItem(`token`, (array['sessionTokenBck']+''));
                  localStorage.setItem(`email`, this.email);
                  this.navCtrl.setRoot(HomePage);   
              }
                  
            },
          err => {
            this.alertCtrl.create({
                        title: "¡Oops!",
                        subTitle: err.error,
                        buttons: ["OK"]
                    }).present();  
          } 
        );
  }; 

}
