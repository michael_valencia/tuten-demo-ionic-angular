
import { NavController, LoadingController, AlertController } from 'ionic-angular';

import { Component, OnInit, SimpleChanges } from '@angular/core';
import { UsuarioProvider } from "../../providers/usuario/usuario";


@Component({
  selector: 'page-home',
  templateUrl: 'home.html' 
})
export class HomePage implements OnInit {

  bookings = [];
  bookingsFilterTable = [];
  filterBookingIdLike = '';
  filterBookingIdMajor = '';
  filterBookingIdMinor = '';

  filterPrecioIdLike = '';
  filterPrecioIdMajor = '';
  filterPrecioIdMinor = '';
 
  constructor(
  	public navCtrl: NavController,
  	public loadingCtrl: LoadingController,
  	private alertCtrl: AlertController,
  	private _us: UsuarioProvider
  	) {
 

  }
 
  ngOnInit() {

  	let loading = this.loadingCtrl.create({
      content:'Cargando información'
    });

    loading.present();
  

    this._us.getBooking()
      .subscribe(
        res => {
            this.bookings = <any>res;
            this.bookingsFilterTable = <any>res;
 
            loading.dismiss();
            console.log(res);
          },
        err => {
         this.alertCtrl.create({
                        title: "¡Oops!",
                        subTitle: err.error,
                        buttons: ["OK"]
                    }).present();
            loading.dismiss();
            console.log(err);   
        }
      );
  }

  filterTable(e) {

          this.bookingsFilterTable = this.bookings.filter(booking => {
            if (  ( (this.filterBookingIdLike != '' && this.filterBookingIdLike != null) ? (booking.bookingId + '').includes(this.filterBookingIdLike) : true )  &&
                  ( (this.filterBookingIdMinor != '' && this.filterBookingIdMinor != null) ? (booking.bookingId <= this.filterBookingIdMinor) : true ) &&
                  ( (this.filterBookingIdMajor != '' && this.filterBookingIdMajor != null) ? (booking.bookingId >= this.filterBookingIdMajor) : true ) &&
                  ( (this.filterPrecioIdLike != '' && this.filterPrecioIdLike != null) ? (booking.bookingPrice + '').includes(this.filterPrecioIdLike) : true )  &&
                  ( (this.filterPrecioIdMinor != '' && this.filterPrecioIdMinor != null) ? (booking.bookingPrice <= this.filterPrecioIdMinor) : true ) &&
                  ( (this.filterPrecioIdMajor != '' && this.filterPrecioIdMajor != null) ? (booking.bookingPrice >= this.filterPrecioIdMajor) : true )
                ) return booking;
          });

  }

}
