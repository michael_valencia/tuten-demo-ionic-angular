import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { URL_SERVICIOS } from "../../config/url.servicios";
import { Http, URLSearchParams } from '@angular/http';
import { AlertController, Platform, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import 'rxjs/add/operator/map'; 

@Injectable()
export class UsuarioProvider {
  
  private httpOptions:any;
  private _loginUrl = URL_SERVICIOS + `testapis%40tuten.cl`;
  private _bookingUrl = URL_SERVICIOS + `contacto%40tuten.cl/bookings?current=true`;

  constructor( 
  	public http: HttpClient,
  	private alertCtrl: AlertController,
    public platform: Platform,
    private storage: Storage,
    public loadingCtrl: LoadingController 
  ) {
    
  }


    loginUser(email:string, password:string)  { 
      this.httpOptions = {
        headers: new HttpHeaders({
          'Accept':  'application/json',
          'password' : password,
          'app' : 'APP_BCK'
        })
      };

      return this.http.put<any>(this._loginUrl, {
            email : email
          }, this.httpOptions);
    }

public email: any;
public token: any;
    getBooking()  { 
        if(this.platform.is("cordova")){
          
                this.storage.ready()
                    .then(()=>{
                      this.email = this.storage.get("email")
                      this.token = this.storage.get("token")
                      console.log(this.email);
                      console.log(this.token);


                      this.httpOptions = {
                        headers: new HttpHeaders({
                          'Accept':  'application/json',
                          'adminemail' : this.email,
                          'token' : this.token,
                          'app' : 'APP_BCK'
                        })
                      };
                    })

            }else{
                this.httpOptions = {
                  headers: new HttpHeaders({
                    'Accept':  'application/json',
                    'adminemail' : localStorage.getItem('email'),
                    'token' : localStorage.getItem('token'),
                    'app' : 'APP_BCK'
                  })
                };
            }

      return this.http.get<any>(this._bookingUrl, this.httpOptions);
    } 

}
