import { Component } from '@angular/core';
import { Platform, MenuController, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';

import { UsuarioProvider } from '../providers/usuario/usuario';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  inicio = HomePage; 
  rootPage:any = LoginPage;

   constructor(
    platform: Platform, 
    statusBar: StatusBar, 
    splashScreen: SplashScreen,
    private menuCtrl: MenuController,
    public _us: UsuarioProvider,
    private alertCtrl:AlertController
    ) {
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }


  abrirPagina( pagina:any){
    this.rootPage = pagina;
    this.menuCtrl.close();
  }

  seguro_cerrar_sesion(){
    this.alertCtrl.create({
      title: "¡Atención!",
      message: "¿Desea cerrar sesión?",
      buttons: [
        {
          text: 'Cancelar'
        },
        {
          text: 'Aceptar',
          handler: () => {
            this.cerrar_sesion();
          }
        }
      ]
    }).present();
  }

 
 cerrar_sesion(){ 
    this.rootPage = LoginPage;
    this.menuCtrl.close();
  }

}

