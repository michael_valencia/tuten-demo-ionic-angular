
## Componentes utilizados 

Ionic 3
Angular 5
Node v10.13


## Ejecutar proyecto

Clonar de: git clone https://michael_valencia@bitbucket.org/michael_valencia/tuten-demo-ionic-angular.git
Ejecutar: npm install -g ionic cordova
Ejecutar: npm install
Ejecutar: ionic cordova platform add android
Ejecutar: ionic serve en la consola de comandos. Navegar a http://localhost:8100. 
